#include "cartridge.h"

namespace rbnes{

/*
An iNES file consists of the following sections, in order:

Header (16 bytes)
Trainer, if present (0 or 512 bytes)
PRG ROM data (16384 * x bytes)
CHR ROM data, if present (8192 * y bytes)
PlayChoice INST-ROM, if present (0 or 8192 bytes)
PlayChoice PROM, if present (16 bytes Data, 16 bytes CounterOut) (this is often missing, see PC10 ROM-Images for details)
Some ROM-Images additionally contain a 128-byte (or sometimes 127-byte) title at the end of the file.

The format of the header is as follows:

0-3: Constant $4E $45 $53 $1A ("NES" followed by MS-DOS end-of-file)
4: Size of PRG ROM in 16 KB units
5: Size of CHR ROM in 8 KB units (Value 0 means the board uses CHR RAM)
6: Flags 6 - Mapper, mirroring, battery, trainer
7: Flags 7 - Mapper, VS/Playchoice, NES 2.0
8: Flags 8 - PRG-RAM size (rarely used extension)
9: Flags 9 - TV system (rarely used extension)
10: Flags 10 - TV system, PRG-RAM presence (unofficial, rarely used extension)
11-15: Unused padding (should be filled with zero, but some rippers put their name across bytes 7-15)

*/

  namespace {
    // Byte offsets of the cartridge rom
    const uint16_t PROG_ROM_PAGE_COUNT_A = 4; // Count in 16KiB blocks
    const uint16_t CHAR_ROM_PAGE_COUNT_A = 5; // Count in 8KiB blocks
    const uint16_t FLAGS_6_A             = 6;
    const uint16_t FLAGS_7_A             = 7;
    const uint16_t HEADER_SIZE           = 16;
    const uint16_t TRAINER_SIZE          = 512;
    const uint16_t PROG_ROM_PAGE_SIZE    = 16384;
    const uint16_t CHAR_ROM_PAGE_SIZE    = 8192;
  }


  Cartridge::Cartridge(const std::string& file_name) :
    m_cart_file(file_name, std::ofstream::in | std::ofstream::binary | std::ios::ate),
    m_ines_data(),
    m_flags6(0),
    m_flags7(0)
  {
    if (!m_cart_file.is_open()) {
      if (file_name.empty()) {
        std::cerr << "No cartridge file specified! rbnes -h for usage\n";
      }
      else {
        std::cerr << "Error opening cartridge file at \"" << file_name << "\"\n";
      }
      exit(ENOENT);
    }

    long int cart_size = m_cart_file.tellg();
    if (cart_size > MAX_INES_SIZE) { 
      std::cerr << "Error opening cartridge file, greater than 64MiB\n";
      exit(EFBIG);
    }
    m_ines_data.resize(cart_size);


    m_cart_file.seekg(0);
    m_cart_file.read(reinterpret_cast<char*>(&m_ines_data[0]), cart_size);
    if(m_cart_file.gcount() == cart_size){
      std::cout << "Successfully loaded cartridge file.\n";
    }

    m_flags6 = m_ines_data[FLAGS_6_A];
    m_flags7 = m_ines_data[FLAGS_7_A];
  
  }


  /* FLAGS 6
    76543210
    ||||||||
    |||||||+- Mirroring: 0: horizontal (vertical arrangement) (CIRAM A10 = PPU A11)
    |||||||              1: vertical (horizontal arrangement) (CIRAM A10 = PPU A10)
    ||||||+-- 1: Cartridge contains battery-backed PRG RAM ($6000-7FFF) or other persistent memory
    |||||+--- 1: 512-byte trainer at $7000-$71FF (stored before PRG data)
    ||||+---- 1: Ignore mirroring control or above mirroring bit; instead provide four-screen VRAM
    ++++----- Lower nybble of mapper number */

  MirrorMode Cartridge::get_mirroring(){
    switch(m_flags6 & 0x09){
      case (0x00):
        return MirrorMode::horizontal;
        break;
      case (0x01):
        return MirrorMode::vertical;
      default:
        return MirrorMode::four_screen;
    }
  }

  bool Cartridge::has_nvram(){
    return m_flags6 & 0x2;
  }

  bool Cartridge::has_trainer(){
    return m_flags6 & 0x4;
  }
    
  /* FLAGS 7
    76543210
    ||||||||
    |||||||+- VS Unisystem // Unsupported
    ||||||+-- PlayChoice-10 (8KB of Hint Screen data stored after CHR data) // Unsupported
    ||||++--- If equal to 2, flags 8-15 are in NES 2.0 format // Unsupported
    ++++----- Upper nybble of mapper number */

  byte Cartridge::get_mapper(){
    return ((m_flags6 >> 4) | (m_flags7 & 0xF0));
  }

  std::vector<byte> Cartridge::get_prg_rom(){
    auto prg_start = m_ines_data.begin() + HEADER_SIZE;
    if(has_trainer()){
      prg_start += TRAINER_SIZE;
    }
    
    auto prg_end = prg_start + (m_ines_data[PROG_ROM_PAGE_COUNT_A] * PROG_ROM_PAGE_SIZE);
    return {prg_start, prg_end};
  }

  std::vector<byte> Cartridge::get_char_rom(){
    auto char_start = m_ines_data.begin() + HEADER_SIZE;
    if(has_trainer()){
      char_start += TRAINER_SIZE;
    }
    
    auto char_end = char_start + (m_ines_data[CHAR_ROM_PAGE_COUNT_A] * CHAR_ROM_PAGE_SIZE);
    return std::vector<byte>(char_start, char_end);
  }

  /* Flags 8 through 10 are ignored since they're not widely used */
  /* FLAGS 8 - Program ram size in 8KiB blocks */
  /* FLAGS 9 
    76543210
    ||||||||
    |||||||+- TV system (0: NTSC; 1: PAL)
    +++++++-- Reserved, set to zero */
  /* FLAGS 10 
    76543210
      ||  ||
      ||  ++- TV system (0: NTSC; 2: PAL; 1/3: dual compatible)
      |+----- PRG RAM ($6000-$7FFF) (0: present; 1: not present)
      +------ 0: Board has no bus conflicts; 1: Board has bus conflicts */



}
