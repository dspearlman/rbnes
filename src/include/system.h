#ifndef __SYSTEM_H_ 
#define __SYSTEM_H_
#include "RBNES.h"
#include "cartridge.h"
#include <functional>
#include "mapper.h"

namespace rbnes{

  class rbnes_system {
    public:
      rbnes_system(Cartridge& ines);
      /* Write something to the system bus from CPU */
      void cpu_write_memory(const cpu_addr_t address, const byte data);

      /* Read something from the system bus to CPU */
      byte cpu_read_memory(const cpu_addr_t address);

      /* CPU must call this every emulated cycle to keep the system in sync */
      void cpu_tick(){};

      /* Lambdas for reading and writing PPU registers */
      void set_ppu_rw(std::function<byte (ppu_addr_t)> read, std::function<void (ppu_addr_t, byte)> write);

    private:
      //memory
      byte m_internal_ram[INTERNAL_MEM_SIZE];
      std::unique_ptr<Mapper> m_mapper;

      /* Lambdas for reading and writing PPU registers */
      std::function<byte (ppu_addr_t)> m_ppu_read_reg;
      std::function<void (ppu_addr_t, byte)> m_ppu_write_reg;
  };
  using rbnes_system_t = std::shared_ptr<rbnes_system>; 
}

#endif
