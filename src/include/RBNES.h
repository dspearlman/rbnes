/* This file is the "global include" file. Any header in namespace rbnes should probably include this first. 
 * Mostly types and aliases are defined here, and headers like <stdint> that are neaded throughout the project.
 */

#ifndef __RBNES_H_
#define __RBNES_H_

#include <stdint.h>
#include <memory>
#include <cerrno>

namespace rbnes {
  using cpu_addr_t = uint16_t;
  using ppu_addr_t = uint16_t;
  using byte = uint8_t;

  enum class Cpu_Interrupt {
    IRQ,
    NMI,
    BRK
  };
  const long int MAX_INES_SIZE = 67108864; // 64MiB 
  const size_t INTERNAL_MEM_SIZE = 2048;
}

#endif
