#ifndef __MAPPER_H_
#define __MAPPER_H_

#include "RBNES.h"
#include <vector>
#include <cstdlib>

namespace rbnes{

  /* Base class for all mappers. Implements M0 (no mapper/NROM) */
  class Mapper{
    public:
      /* Factory method to create mappers */
      static std::unique_ptr<Mapper> create_mapper(std::vector<byte> prg_rom, byte mapper_id){
      switch(mapper_id){
        case 0:
          return std::make_unique<Mapper>(Mapper(prg_rom));
          break;
        default:
          fprintf(stderr, "Unsupported mapper type: %u\n", mapper_id);
          exit(ENOTSUP);
          break;
        }
      return nullptr;
      }

      /* Constructor for M0 (no mapper/NROM)
       *  This mode either maps the lower 16KiB to 0x8000 and the upper 16KiB to 0xC000 for 32KiB roms,
       *  or mirrors 0x8000-0xBFFF to 0xC000-0xFFFF for 16KiB roms */
      Mapper(std::vector<byte> prg_rom);

      /* Destructor */
      virtual ~Mapper() = default;

      /* Read a byte from mapped memory */
      virtual byte read(const cpu_addr_t addr);

      /* Attempt to write a byte. ROM writes fail */
      virtual void write(const cpu_addr_t addr, byte data){(void)addr; (void)data;}

    protected:
      /* All mappers should have access to the program rom */
      std::vector<byte> m_prg_rom;

    private: 
      /* M0 implementation specific members */ 
      inline uint16_t address_translate(const cpu_addr_t);

      bool m_is_32K;
  };
}
#endif
