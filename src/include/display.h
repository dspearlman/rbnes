#ifndef __DISPLAY_H_
#define __DISPLAY_H_

#include <SDL2/SDL.h>
namespace rbnes {

  class Display {

    SDL_Window* m_window;
    SDL_Surface* m_screen_surface;
  public:
    Display();
    ~Display();

  };
}


#endif
