/* Cartridge class definition
 *
 * This class takes care of loading in an iNES ROM and extracting relevent data.
 */


#ifndef __CARTRIDGE_H_
#define __CARTRIDGE_H_

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "RBNES.h"
namespace rbnes {

  enum class MirrorMode {
    horizontal, // CIRAM A10 = PPU A11 
    vertical,   // CIRAM A10 = PPU A10
    four_screen
  };


  class Cartridge {
    
  public:

    Cartridge(const std::string& file_name);

    /* Get the program ram */
    std::vector<byte> get_prg_rom();

    /* Get the character rom */
    std::vector<byte> get_char_rom();

    /* Get the memory mirroring mode */
    MirrorMode get_mirroring();

    /* Has battery backed or persistant memory */
    bool has_nvram();

    /* Returns the mapper number */
    byte get_mapper();




  private:
    /* Returns whether or not the iNES file has a 512 bit trainer present */
    bool has_trainer();

    std::ifstream m_cart_file;
    std::vector<byte> m_ines_data;

    byte m_flags6;
    byte m_flags7;
  };


}

#endif

