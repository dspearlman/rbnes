/* CPU6502 class definition
 *
 * The NES uses a RICOH 2A03 processor, which contains a MOS 6502 CPU (decimal mode disabled), an Audio Processing Unit (APU),
 * and some memory mapped IO. Since we're in software land, we don't need to worry about what components are on what chip, so
 * the pieces are implemented separately.
 */


// CPU is in charge of time synchronization. All memory access times are one cycle, which simplifies things a bit.
// CPU owns the system (passed in at construction), which handles memory mapping and owns the other components.
// Although this is slightly counter intuitive, since the CPU is actually part of the system, this organization makes
// things easier to port to Rust if I ever decide to do so.
//
//
//
//  
//                    //6502 has a bug such that the when the vector of an indirect address begins at the last byte of a page,
//                    //the second byte is fetched from the beginning of that page rather than the beginning of the next
//  
//


#ifndef __CPU6502_H_
#define __CPU6502_H_

#include "RBNES.h"
#include "system.h"

namespace rbnes {
  
  // Masks for setting and clearing flag bits. AND the CLR values for clear, OR the mask values for set/get.
  namespace FLAGS {
    using mask_t = struct{
      const byte mask;
      const byte clear;
    };
    const mask_t N = {0b10000000, 0b01111111}; // Negative
    const mask_t V = {0b01000000, 0b10111111}; // oVerflow
    const mask_t A = {0b00100000, 0b11011111}; // Always 1 (reserved)
    const mask_t B = {0b00010000, 0b11101111}; // Push source "B flag" (set for PHP and BRK)
    const mask_t D = {0b00001000, 0b11110111}; // Decimal mode
    const mask_t I = {0b00000100, 0b11111011}; // Interrupt disable
    const mask_t Z = {0b00000010, 0b11111101}; // Zero 
    const mask_t C = {0b00000001, 0b11111110}; // Carry 
  }

  class Cpu_6502 {

  public : 
    
    Cpu_6502(rbnes_system_t system);

    /* Runs the emulator 5ever. 
     */
    void run();

    /* Pass an interrupt to the CPU
     */
    void interrupt(Cpu_Interrupt interrupt_type);

    void force_instruction(byte instruction, byte operand=0, uint32_t address=0);

  private:

    const cpu_addr_t INTERRUPT_VECTOR = 0xFFFE;

    /*** System bus and other components ***/
    rbnes_system_t m_system;

    /*** Registers (Wow there are so few!) ***/
    
    // Status flags
    byte m_reg_flags; // NVxBxIZC Negative oVerflow Break Interrupt-disable Zero Carry

    //Program Counter
    uint16_t m_reg_pc;
    
    //Stack Pointer
    byte m_reg_sp;

    //Accumulator A
    byte m_reg_a;

    //Index Registers X and Y
    byte m_reg_x, m_reg_y;


    /*** Internal State ***/
    uint32_t m_cpu_cycles;

    uint32_t m_bad_ops;



    /*** Helpers ***/
    
    // Informs the system that a CPU cycle has finished
    void tick();
    void tick(byte num);
    
    // Giant case statement =D
    void decode_execute(byte op);

    // Call these when accessing memory to remain in sync with the rest of the system
    byte cpu_read_memory(const cpu_addr_t address);
    void cpu_write_memory(const cpu_addr_t address, const byte data);

    // Reads two bytes forming an address
    cpu_addr_t read_address(const cpu_addr_t address);
    // Read two bytes pointed at by the PC forming an address and increments the PC by two
    cpu_addr_t read_address_pc();

    // Check to see if indexing causes a page cross, adds a cycle if it does
    void check_page_cross(const cpu_addr_t address, cpu_addr_t idx, bool enable);

    // Addressing modes. CPU cycles are added when applicable. * means +1 if page crossed
    // Additional cycles are added when the calling code reads or writes at the returned address
    cpu_addr_t zero_page();                            // +1 cycles
    cpu_addr_t zero_page_index_x();                    // +2 cycles
    cpu_addr_t zero_page_index_y();                    // +2 cycles
    cpu_addr_t absolute();                             // +2 cycles
    cpu_addr_t absolute_index_x(bool check_crossing);  // +2 cycles *
    cpu_addr_t absolute_index_y(bool check_crossing);  // +2 cycles *
    cpu_addr_t pre_indexed();                          // +4 cycles
    cpu_addr_t indirect_indexed(bool check_crossing);  // +3 cycles *
    void relative();                                   // +1 cycles *
    // Relative addressing is only used when a branch instruction succeeds, void is fine here
    // Indirect mode for jump is the same as read_address(read_address)

    //// Setting flags
    
    // Updates the negative and zero flags based on the passed in result
    void check_sign(byte data);
    void check_zero(byte data);
    void set_flag(bool condition, const FLAGS::mask_t& flag); // Set flag when condition is true, otherwise clear
    byte get_flag(const FLAGS::mask_t& flag);
    void clr_flag(const FLAGS::mask_t& flag);

    /*** Instructions ***/
    
    /***** ALU instruction mnemonics *****/
    
  
    //  ADC         Add memory to accumulator with carry           N Z C I D V              
    //  Operation:  A + M + C -> A, C                              * * *     *    
    void ADC(cpu_addr_t address);

    //  AND         Logical AND to the accumulator                 N Z C I D V
    //  Operation:  A & M -> A                                     * *
    void AND(cpu_addr_t address);

    //  "OR" Memory with Accumulator                               N Z C I D V  
    //  Operation:  A & M -> A                                     * *
    void ORA(cpu_addr_t address);

    // EOR           Exclusive-Or Memory with Accumulator          N Z C I D V
    //  Operation:  A EOR M -> A                                   * *
    void EOR(cpu_addr_t address);

    //  ASL          Shift Left One Bit (Memory or Accumulator)    N Z C I D V
    //  Operation:   C <- |7|6|5|4|3|2|1|0| <- 0                   * * *
    byte  asl(byte operand);
    void  ASL(cpu_addr_t address);
    void  ASL_A();

    //  LSR          Shift Right One Bit (Memory or Accumulator)   N Z C I D V   
    //  Operation:  0 -> |7|6|5|4|3|2|1|0| -> C                    0 * *
    byte lsr(byte operand);
    void LSR(cpu_addr_t address);
    void LSR_A();

    //  BIT          Test bits in memory with accumulator          N Z C I D V
    //  Operation:   A & M, M7 -> N, M6 -> V                       * *       *
    void BIT(cpu_addr_t address); 
    
    //  CMP CPX CPY
    //  CMPAXY      Compare Memory and Register                    N Z C I D V
    //  Operation:  R - M                                          * * *
    void CMPAXY(byte& reg, cpu_addr_t address);

    //  DEC          Decrement Memory by One                       N Z C I D V
    //  Operation:   M - 1 -> M                                    * *
    void DEC(cpu_addr_t address);

    //  DEX(Y)       Decrement Index X or Y by One                 N Z C I D V
    //  Operation:   X - 1 -> X                                    * *
    void DEXY(byte& index_reg);

    //  INC          Increment Memory by One                       N Z C I D V
    //  Operation:  M + 1 -> M                                     * *
    void INC(cpu_addr_t address);

    //  INX(Y)       Increment index reg by One                    N Z C I D V
    //  Operation:  M + 1 -> M                                     * *
    void INXY(byte& reg);

    //  ROL          Rotate Memory or Accumulator One Bit Left     N Z C I D V 
    //               |------------------------------|              * * *
    //  Operation:   +<- |7|6|5|4|3|2|1|0| <- |C| <-+  
    //  *** TODO Verify the timing after change
    byte rol(byte operand);
    void ROL(cpu_addr_t address);
    void ROL_A();

    //  ROR          Rotate Memory or Accumulator One Bit Right    N Z C I D V 
    //               |------------------------------|              * * *
    //  Operation:   +-> |7|6|5|4|3|2|1|0| -> |C| ->+   
    byte ror(byte operand);
    void ROR(cpu_addr_t address);
    void ROR_A();

    //  SBC          Subtract Memory from Accumulator with Borrow  N Z C I D V
    //  Operation: A - M - C -> A                                  * * *     *
    void SBC(cpu_addr_t address);

    /***** Load/Store/transfer instructions *****/
                          
    //  LDA LDX LDY   Load Reg with Memory                          N Z C I D V
    //  Operation: M -> R                                           * * 
    void LDR(byte& reg, cpu_addr_t address);

    //  STA STX STY Store Register in Memory (no flags affected)                                 
    /*** Takes one extra cycle for 5 Absolute indexed and indirect */
    //  page crossing ignored 
    //  
    void STR(cpu_addr_t addr, byte reg);

    // Transfer instructions only take one execution cycle, and normally affect NZ flags
    void TXR(byte& dest, const byte source);
    // TAX Transfer Accumulator to Index X                              
    // TAY Transfer Accumulator to Index Y                              
    // TSX Transfer Stack Pointer to Index X                            
    // TXA Transfer Index X to Accumulator                              
    // TXS Transfer Index X to Stack Pointer (Does not affect flags)                            
    // TYA Transfer Index Y to Accumulator                              
          
    /***** Branch/Jump instructions ******
    * Branch instructions all use relative addressing, add a cycle
    * for page crossing, and add another cycle if the branch is taken. */

    // BCC Branch on Carry Clear
    // BCS Branch on Carry Set
    // BEQ Branch on Result Zero
    // BMI Branch on Result Minus
    // BNE Branch on Result not Zero
    // BPL Branch on Result Plus
    // BVC Branch on Overflow Clear
    // BVS Branch on Overflow Set
    // BCC BCS BEQ BMI BNE BPL are all branch_if_set/clr
    
    void branch_if_set(FLAGS::mask_t flag);
    void branch_if_clr(FLAGS::mask_t flag);


    // Jump to New Location 
    void JMP_A(); //absolute
    void JMP_I(); //indirect
    // Jump to New Location Saving Return Address 
    void JSR();
   
    /***** Flag instructions *****/
    
    // Clear Carry Flag
    void CLC();
    // Clear Decimal Mode (Not supported, but flag apparently still needs to respond)
    void CLD();
    // Clear interrupt Disable Bit
    void CLI();
    // Clear Overflow Flag                                   
    void CLV();
    // Set Carry Flag                                               
    void SEC();
    // Set Decimal Mode (Not supported, but flag apparently still needs to respond)
    void SED();
    // Set Interrupt Disable Status     
    void SEI();

    /***** Stack instructions *****/

    // Push reg on Stack
    // PHA and PHP are both Push, 
    // PLA and PLP are both Pop
    void PUSH(byte reg);
    void POP(byte& reg);

    /***** Misc instructions *****/
    // No Operation                                                 
    void NOP(){tick();}
    // Force Break
    void BRK();
    // Return from Interrupt 
    void RTI();
    // Return from Subroutine 
    void RTS();
                                                       
  };
}

#endif
