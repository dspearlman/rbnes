#ifndef __PPU_H_
#define __PPU_H_

#include <functional>

#include "RBNES.h"
#include "system.h"

namespace rbnes {
  namespace{
    const size_t NUM_PPU_REGISTERS = 5;
    const int PPUCTRL   = 0;
    const int PPUMASK   = 1;
    const int PPUSTATUS = 2;
    const int OAMADDR   = 3;
    const int OAMDATA   = 4;
    const int PPUSCROLL = 5;
    const int PPUADDR   = 6;
    const int PPUDATA   = 7;
  }
  class Ppu {

  public:
    Ppu (rbnes_system_t system);

    /* Addressing of the pattern table is as follows:
    DCBA98 76543210
    ---------------
    0HRRRR CCCCPTTT
    |||||| |||||+++- T: Fine Y offset, the row number within a tile
    |||||| ||||+---- P: Bit plane (0: "lower"; 1: "upper")
    |||||| ++++----- C: Tile column
    ||++++---------- R: Tile row
    |+-------------- H: Half of sprite table (0: "left"; 1: "right")
    +--------------- 0: Pattern table is at $0000-$1FFF
    */


  private:
    // Registers
    byte m_reg_ppuctrl;
    byte m_reg_ppumask;
    byte m_reg_ppustatus;
    byte m_reg_oamaddr;
    byte m_reg_oamdata;
    byte m_reg_ppuscroll_x;
    byte m_reg_ppuscroll_y;
    bool ppu_scroll_select;
    byte m_reg_ppuaddr_h;
    byte m_reg_ppuaddr_l;
    bool m_reg_ppuaddr_select;


    byte ppu_read_reg(ppu_addr_t);
    void ppu_write_reg(ppu_addr_t, byte);

    rbnes_system_t m_system;
  };
}


#endif
