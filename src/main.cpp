#include "CPU6502.h"
#include "cartridge.h"
#include "system.h"
#include "ppu.h"
#include "display.h"
#include <cstdio>
using namespace rbnes;


int main() {
  auto display = Display();
  auto ines = Cartridge("nestest.nes");
  auto sys = std::make_shared<rbnes_system>(ines);
  auto cpu = Cpu_6502(sys);
  auto ppu = Ppu(sys);
  cpu.run();
  return 0;
}

