#include "CPU6502.h"
#include <iostream>
#include <cstdio>
#include <SDL2/SDL.h>


namespace rbnes{
  //Addressing modes
  
  Cpu_6502::Cpu_6502(rbnes_system_t system) 
    : m_system(system),
      m_reg_flags(0b00100100), // Reserved bit is always 1, start with interrupts disabled
      m_reg_pc(0xC000),
      m_reg_sp(0xFD),
      m_reg_a(0),
      m_reg_x(0),
      m_reg_y(0),
      m_cpu_cycles(7), // Start at cycle 7 to pretend there was some init code
      m_bad_ops(0)
  {}

  void Cpu_6502::run(){
    byte opcode;
    unsigned int i = 0;
    while(i < 8993){
      ++i;
      opcode = cpu_read_memory(m_reg_pc++); // Fetch
      printf("\n%04X op: 0x%02X    A:%02X X:%02X Y:%02X P:%02X SP:%02X CYC:%u    ",  m_reg_pc - 1, opcode, m_reg_a, m_reg_x, m_reg_y, m_reg_flags, m_reg_sp, (m_cpu_cycles - 1));

      decode_execute(opcode);


      // Check for interrupts
    }
  }

  // There's a very good chance the compiler will ignore my request to inline all these, 
  // but I'm giving it a hint since each opcode deserves its own monolithic function.
  // gcc 8.2 will inline with -O1 but only with the inline keyword, so I guess it does matter.
  // It inlines without the inline when -O3 is used. (Check out godbolt.org, super useful 
  // for quickly checking compiler output)
  //
  // I've seen in some other code the use of templates for function compositions to avoid code
  // duplication, but I find it a bit awkward to read. I don't have a lot of templating
  // experience so there may be a little bias, but IMHO using functions for function
  // composure is more universally readable, and the compiler generates similar (or maybe 
  // even the same) code since everything is inlined anyway. If someone has an argument
  // for the template style, I'd love to hear it. There may be some cool interesting reasoning
  // behind it that I'm unaware of.
    
  /*************************************************
   * ********** Helper functions ***************** *
   *************************************************/

  // Informs the system that a CPU cycle has finished
  inline void Cpu_6502::tick(){ m_cpu_cycles++; m_system->cpu_tick();}
  inline void Cpu_6502::tick(byte num){ while(num--) { m_cpu_cycles++; m_system->cpu_tick();}}

  // All memory accesses on the 6502 take one cycle. Talk about simplicity!
  inline byte Cpu_6502::cpu_read_memory(const cpu_addr_t address){
    tick(); 
    return m_system->cpu_read_memory(address);
  }
  inline void Cpu_6502::cpu_write_memory(const cpu_addr_t address, const byte data){
    tick(); 
    if(address == 0x02 || address == 0x03){ // TODO Temporary for debug mode, later enable as a test option for unit testing
      printf("Address 0x%02X: %02X \n", address, data);
      exit(1);
    }
    m_system->cpu_write_memory(address, data);
  }

  // Reads two bytes forming an address stored in Little Endian
  inline cpu_addr_t Cpu_6502::read_address(const cpu_addr_t address){
    return cpu_read_memory(address) | (cpu_read_memory(address+1) << 8);
  }
  // Read two bytes pointed at by the PC forming an address in Little Endian and increments the PC by two
  inline cpu_addr_t Cpu_6502::read_address_pc(){ 
    m_reg_pc += 2; return (read_address(m_reg_pc-2));
  }

  // Check to see if indexing causes a page cross, adds a cycle if it does
  inline void Cpu_6502::check_page_cross(const cpu_addr_t address, cpu_addr_t idx, bool enable=true) {
    if (enable && (((address + idx) & 0xFF00) != (address & 0xFF00))) {
      tick();
    }
  }


  /*************************************************
   * ********** Addressing Modes ***************** *
   *************************************************/
  // CPU cycles are added when applicable.
  
  inline cpu_addr_t Cpu_6502::zero_page(){ 
    return (cpu_read_memory(m_reg_pc++));
  }

  inline cpu_addr_t Cpu_6502::zero_page_index_x(){
    tick();  // Extra tick for addition. This assumes in the real system that old 
             // values should be read on the same cycle new values are written, but I haven't verified.
    return ((cpu_read_memory(m_reg_pc++) + m_reg_x) & 0xFF);
  } 

  inline cpu_addr_t Cpu_6502::zero_page_index_y(){ 
    tick();  //Extra tick for addition
    return ((cpu_read_memory(m_reg_pc++) + m_reg_y) & 0xFF);
  }

  inline cpu_addr_t Cpu_6502::absolute(){ 
    return (read_address_pc());
  }

  inline cpu_addr_t Cpu_6502::absolute_index_x(bool check_crossing=true){
    cpu_addr_t address = read_address_pc();
    check_page_cross(address, m_reg_x, check_crossing); 
    return (address + m_reg_x);
  }

  inline cpu_addr_t Cpu_6502::absolute_index_y(bool check_crossing=true){ 
    cpu_addr_t address = read_address_pc();
    check_page_cross(address, m_reg_y, check_crossing); 
    return (address + m_reg_y);
  }

  inline cpu_addr_t Cpu_6502::pre_indexed(){
    byte arg = cpu_read_memory(m_reg_pc++);
    byte lower_address_byte = cpu_read_memory((arg + m_reg_x) & 0xFF);
    byte upper_address_byte = cpu_read_memory((arg + m_reg_x + 1) & 0xFF);
    tick();
    return (lower_address_byte | (upper_address_byte << 8));
  }

  inline cpu_addr_t Cpu_6502::indirect_indexed(bool check_crossing=true){
    cpu_addr_t arg = cpu_read_memory(m_reg_pc++);
    byte lower_address_byte = cpu_read_memory(arg & 0xFF);
    byte upper_address_byte = cpu_read_memory((arg+1) & 0xFF);
    cpu_addr_t target = lower_address_byte | (upper_address_byte << 8);
    check_page_cross(target, m_reg_y, check_crossing);
    printf("//target %04X ", target);
    return (target + m_reg_y);
  }

  inline void Cpu_6502::relative(){
    // Branch offset is signed
    byte offset = cpu_read_memory(m_reg_pc++);
    cpu_addr_t target = m_reg_pc + offset;
    check_page_cross(m_reg_pc, target);
    m_reg_pc = target;
  }

  /*********************************************
   * ************ Flag related *************** *
   *********************************************/
  inline void Cpu_6502::check_sign(byte data){
    set_flag((data & 0x80), FLAGS::N);
  }

  inline void Cpu_6502::check_zero(byte data){
    set_flag(!(data), FLAGS::Z);
  }

  inline void Cpu_6502::set_flag(bool condition, const FLAGS::mask_t& flag){
    m_reg_flags = condition ? (m_reg_flags | flag.mask) : (m_reg_flags & flag.clear);
  }

  inline byte Cpu_6502::get_flag(const FLAGS::mask_t& flag){
    printf("//flag: %X ", m_reg_flags & flag.mask ? 1 : 0); // TODO Temporary for debug
    return (m_reg_flags & flag.mask) ? 1 : 0;
  }

  inline void Cpu_6502::clr_flag(const FLAGS::mask_t& flag){
    m_reg_flags &= flag.clear;
  }



    

  
  /*********************************************
   * ************ Instructions *************** *
   *********************************************/
                                             
  inline void Cpu_6502::ADC(cpu_addr_t address){
    byte operand = cpu_read_memory(address);
    uint32_t sum = operand + m_reg_a + get_flag(FLAGS::C);
    set_flag((sum & 0x100), FLAGS::C);
    set_flag(((m_reg_a ^ sum) & (operand ^ sum) & 0x80), FLAGS::V);
    m_reg_a = static_cast<byte>(sum);
    check_sign(m_reg_a);
    check_zero(m_reg_a);
  }

  inline void Cpu_6502::AND(cpu_addr_t address){
    m_reg_a &= cpu_read_memory(address);
    check_sign(m_reg_a);
    check_zero(m_reg_a);
  }

  inline void Cpu_6502::ORA(cpu_addr_t address){
    m_reg_a |= cpu_read_memory(address);
    check_sign(m_reg_a);
    check_zero(m_reg_a);
  }

  inline void Cpu_6502::EOR(cpu_addr_t address){
    m_reg_a ^= cpu_read_memory(address);
    check_sign(m_reg_a);
    check_zero(m_reg_a);
  }

  inline byte Cpu_6502::asl(byte operand){
    set_flag((operand & 0x80), FLAGS::C);
    operand = (operand << 1);
    check_sign(operand);
    check_zero(operand);
    return operand;
  }
  inline void Cpu_6502::ASL_A(){
    tick();
    m_reg_a = asl(m_reg_a);
  }

  inline void Cpu_6502::ASL(cpu_addr_t address){
    tick(1);
    cpu_write_memory(address,asl(cpu_read_memory(address)));
  }

  inline byte Cpu_6502::lsr(byte operand){
    set_flag((operand & 0x01), FLAGS::C);
    clr_flag(FLAGS::N);
    operand = operand >> 1;
    check_zero(operand);
    return operand;
  }

  inline void Cpu_6502::LSR_A(){
    tick();
    m_reg_a = lsr(m_reg_a);
  }

  inline void Cpu_6502::LSR(cpu_addr_t address){
    tick(1);
    cpu_write_memory(address, lsr(cpu_read_memory(address)));
  }

  inline void Cpu_6502::BIT(cpu_addr_t address){
    byte operand = cpu_read_memory(address);
    check_sign(operand);
    set_flag((operand & 0x40) , FLAGS::V);
    set_flag(!(m_reg_a & operand), FLAGS::Z);
  }

  inline void Cpu_6502::CMPAXY(byte& reg, cpu_addr_t address){
    uint32_t diff = reg - cpu_read_memory(address);
    set_flag(!(diff & 0x100), FLAGS::C);
    check_sign(static_cast<byte>(diff));
    check_zero(static_cast<byte>(diff));
  }

  inline void Cpu_6502::DEC(cpu_addr_t address){
    tick(); // Extra cycle from datasheet 
    byte operand = cpu_read_memory(address);
    check_sign(--operand);
    check_zero(operand);
    cpu_write_memory(address, operand);
  }

  inline void Cpu_6502::DEXY(byte& index_reg){
    tick();
    check_sign(--index_reg);
    check_zero(index_reg);
  }

  inline void Cpu_6502::INC(cpu_addr_t address){
    tick(); // Extra cycle from datasheet
    byte operand = cpu_read_memory(address);
    check_sign(++operand);
    check_zero(operand);
    cpu_write_memory(address, operand);
  }
    
  inline void Cpu_6502::INXY(byte& reg){
    tick();
    check_sign(++reg);
    check_zero(reg);
  }

  inline byte Cpu_6502::rol(byte operand){
    set_flag((operand & 0x80), FLAGS::C);
    operand = (operand << 1) | (operand >>(7));
    check_sign(operand);
    check_zero(operand);
    return operand;
  }

  inline void Cpu_6502::ROL_A(){
    tick();
    m_reg_a = rol(m_reg_a);
  }
    
  inline void Cpu_6502::ROL(cpu_addr_t address){
    tick(1);
    cpu_write_memory(address, rol(cpu_read_memory(address)));
  }
    
  inline byte Cpu_6502::ror(byte operand){
    byte retv = (operand >> 1) | (get_flag(FLAGS::C) << (7));
    set_flag((operand & 0x01), FLAGS::C);
    check_sign(retv);
    check_zero(retv);
    return retv;
  }

  inline void Cpu_6502::ROR_A(){
    tick();
    m_reg_a = ror(m_reg_a);
  }

  inline void Cpu_6502::ROR(cpu_addr_t address){
    tick(1);
    cpu_write_memory(address, ror(cpu_read_memory(address)));
  }

  inline void Cpu_6502::SBC(cpu_addr_t address){
    byte operand = cpu_read_memory(address);
    uint32_t diff = m_reg_a - operand - !get_flag(FLAGS::C);
    set_flag(!(diff & 0x100), FLAGS::C);
    set_flag(((m_reg_a ^ diff) & (~operand ^ diff) & 0x80), FLAGS::V);
    m_reg_a = static_cast<byte>(diff);
    check_sign(m_reg_a);
    check_zero(m_reg_a);
  }

  inline void Cpu_6502::LDR(byte& reg, cpu_addr_t address){
    reg = cpu_read_memory(address);
    check_sign(reg);
    check_zero(reg);
  }

  inline void Cpu_6502::STR(cpu_addr_t addr, byte reg){
    cpu_write_memory(addr, reg);
  }

  inline void Cpu_6502::TXR(byte& dest, const byte source){
    tick();
    dest = source;
    check_sign(dest);
    check_zero(dest);
  }

  inline void Cpu_6502::branch_if_set(FLAGS::mask_t flag){
    if (get_flag(flag)){
      relative();
    }
    else{
      tick();
      m_reg_pc++;
    }
  }
  inline void Cpu_6502::branch_if_clr(FLAGS::mask_t flag){
    if (!get_flag(flag)){
      relative();
    }
    else{
      tick();
      m_reg_pc++;
    }
  }

  inline void Cpu_6502::JMP_I(){
    cpu_addr_t addr = read_address(m_reg_pc);
    // 6502 bug: when the vector of an indirect address begins at the last byte of a page, the 
    // second byte is fetched from the beginning of that page instead of the beginning of the next
    cpu_addr_t page = addr & 0xff00;
    m_reg_pc = cpu_read_memory(addr) | (cpu_read_memory(page | ((addr + 1) & 0xFF)) << 8);
  }

  inline void Cpu_6502::JMP_A(){
    m_reg_pc = read_address(m_reg_pc);
  }

  inline void Cpu_6502::JSR(){
    byte pc_h = (m_reg_pc + 1) >> 8;
    byte pc_l = (m_reg_pc + 1) & 0xFF;
    PUSH(pc_h);
    PUSH(pc_l);
    printf("//Pushing pc+2: 0x%04X ", m_reg_pc + 2);
    tick();
    JMP_A();
  }

  inline void Cpu_6502::CLC(){ clr_flag(FLAGS::C); tick();}
  inline void Cpu_6502::CLI(){ clr_flag(FLAGS::I); tick();}
  inline void Cpu_6502::CLV(){ clr_flag(FLAGS::V); tick();}
  inline void Cpu_6502::CLD(){ clr_flag(FLAGS::D); tick();}
  inline void Cpu_6502::SEC(){ set_flag(true, FLAGS::C); tick();}
  inline void Cpu_6502::SEI(){ set_flag(true, FLAGS::I); tick();}
  inline void Cpu_6502::SED(){ set_flag(true, FLAGS::D); tick();}

  inline void Cpu_6502::PUSH(byte reg){
    cpu_write_memory(0x100 | m_reg_sp, reg);
    --m_reg_sp;
  }

  inline void Cpu_6502::POP(byte& reg){
    tick(); // Extra clock per spec
    reg = cpu_read_memory(0x100 | ++m_reg_sp);
  }

  inline void Cpu_6502::BRK(){
    // Push PC
    PUSH(static_cast<byte>(++m_reg_pc >> 8));
    PUSH(static_cast<byte>(m_reg_pc & 0xFF));
    set_flag(true, FLAGS::B);
    PUSH(m_reg_flags);
    set_flag(true, FLAGS::I);
    m_reg_pc = read_address(INTERRUPT_VECTOR);
    tick();
  }

  inline void Cpu_6502::RTI(){
    // Manually popping because nestest.log has 3 fewer cycles
    m_reg_flags = cpu_read_memory(0x100 | ++m_reg_sp);
    m_reg_flags |= FLAGS::A.mask; 
    byte pc_l, pc_h;
    pc_l = cpu_read_memory(0x100 | ++m_reg_sp);
    pc_h = cpu_read_memory(0x100 | ++m_reg_sp);
    m_reg_pc = (pc_h << 8) | pc_l;
    tick(2);
  }

  inline void Cpu_6502::RTS(){
    byte pc_l, pc_h;
    POP(pc_l);
    POP(pc_h);
    m_reg_pc = ((pc_h << 8) | (pc_l)) + 1;
    printf("//m_reg_pc after rts 0x%04X ", m_reg_pc);
    tick(1);
  }


  inline void Cpu_6502::decode_execute(byte op){
    switch(op){
      case 0x00: BRK(); break;                                           // Break
      case 0x01: ORA(pre_indexed()); break;                              // ORA (Indirect,X)
      case 0x05: ORA(zero_page()); break;                                // ORA Zero Page
      case 0x06: ASL(zero_page()); break;                                // ASL Zero Page
      case 0x08: PUSH(m_reg_flags | FLAGS::B.mask); tick(); break;       // PHP (Always sets B flag)
      case 0x09: ORA(m_reg_pc++); break;                                 // ORA Immediate
      case 0x0A: ASL_A(); break;                                         // ASL Accumulator
      case 0x0D: ORA(absolute()); break;                                 // ORA Absolute
      case 0x0E: ASL(absolute()); break;                                 // ASL Absolute
      case 0x10: branch_if_clr(FLAGS::N); break;                         // BPL
      case 0x11: ORA(indirect_indexed()); break;                         // ORA (Indirect), Y
      case 0x15: ORA(zero_page_index_x()); break;                        // ORA Zero Page, X
      case 0x16: ASL(zero_page_index_x()); break;                        // ASL Zero Page, X
      case 0x18: CLC(); break;                                           // CLC
      case 0x19: ORA(absolute_index_y()); break;                         // ORA (Absolute), Y
      case 0x1D: ORA(absolute_index_x()); break;                         // ORA (Absolute), X
      case 0x1E: ASL(absolute_index_x(false)); tick(); break;            // ASL Absolute, X
      case 0x20: JSR(); break;                                           // JSR
      case 0x21: AND(pre_indexed()); break;                              // AND (Indirect,X)
      case 0x24: BIT(zero_page()); break;                                // BIT Zero Page
      case 0x25: AND(zero_page()); break;                                // AND Zero Page
      case 0x26: ROL(zero_page()); break;                                // ROL Zero Page
      case 0x28: POP(m_reg_flags); m_reg_flags &= FLAGS::B.clear;        // PLP (ignore bit 4)
                 m_reg_flags |= FLAGS::A.mask; tick(1); break;           // PLP (m_reg_flags bit 5 is always 0)
      case 0x29: AND(m_reg_pc++); break;                                 // AND Immediate
      case 0x2A: ROL_A(); break;                                         // ROL Accumulator
      case 0x2C: BIT(absolute()); break;                                 // BIT Absolute
      case 0x2D: AND(absolute()); break;                                 // AND Absolute
      case 0x2E: ROL(absolute()); break;                                 // ROL Absolute
      case 0x30: branch_if_set(FLAGS::N); break;                         // BMI
      case 0x31: AND(indirect_indexed()); break;                         // AND (Indirect), Y
      case 0x35: AND(zero_page_index_x()); break;                        // AND Zero Page, X
      case 0x36: ROL(zero_page_index_x()); break;                        // ROL Zero Page, X
      case 0x38: SEC(); break;                                           // SEC
      case 0x39: AND(absolute_index_y()); break;                         // AND Absolute, Y
      case 0x3D: AND(absolute_index_x()); break;                         // AND Absolute, X
      case 0x3E: ROL(absolute_index_x(false)); tick(); break;            // ROL Absolute, X
      case 0x40: RTI(); break;                                           // RTI
      case 0x41: EOR(pre_indexed()); break;                              // EOR (Indirect,X)
      case 0x45: EOR(zero_page()); break;                                // EOR Zero Page
      case 0x46: LSR(zero_page()); break;                                // LSR Zero Page
      case 0x48: PUSH(m_reg_a); tick(); break;                           // PHA
      case 0x49: EOR(m_reg_pc++); break;                                 // EOR Immediate
      case 0x4A: LSR_A(); break;                                         // LSR Accumulator
      case 0x4C: JMP_A(); break;                                         // JMP Absolute
      case 0x4D: EOR(absolute()); break;                                 // EOR Absolute
      case 0x4E: LSR(absolute()); break;                                 // LSR Absolute
      case 0x50: branch_if_clr(FLAGS::V); break;                         // BVC
      case 0x51: EOR(indirect_indexed()); break;                         // EOR (Indirect), Y
      case 0x55: EOR(zero_page_index_x()); break;                        // EOR Zero Page, X
      case 0x56: LSR(zero_page_index_x()); break;                        // LSR Zero Page, X
      case 0x58: CLI(); break;                                           // CLI
      case 0x59: EOR(absolute()); break;                                 // EOR Absolute
      case 0x5D: EOR(absolute_index_x()); break;                         // EOR Absolute, X
      case 0x5E: LSR(absolute_index_x(false)); tick(); break;            // LSR Absolute, X
      case 0x60: RTS(); break;                                           // RTS
      case 0x61: ADC(pre_indexed()); break;                              // ADC (Indirect, X)
      case 0x65: ADC(zero_page()); break;                                // ADC Zero Page
      case 0x66: ROR(zero_page()); break;                                // ROR Zero Page
      case 0x68: POP(m_reg_a); check_sign(m_reg_a);                      // PLA
                 check_zero(m_reg_a); tick(1); break;                    // PLA
      case 0x69: ADC(m_reg_pc++); break;                                 // ADC Immediate
      case 0x6A: ROR_A(); break;                                         // ROR Accumulator
      case 0x6C: JMP_I(); break;                                         // JMP Indirect
      case 0x6D: ADC(absolute()); break;                                 // ADC Absolute
      case 0x6E: ROR(absolute()); break;                                 // ROR Absolute
      case 0x70: branch_if_set(FLAGS::V); break;                         // BVS
      case 0x71: ADC(indirect_indexed()); break;                         // ADC (Indirect), Y
      case 0x75: ADC(zero_page_index_x()); break;                        // ADC Zero Page, X
      case 0x76: ROR(zero_page_index_x()); break;                        // ROR Zero Page, X
      case 0x78: SEI(); break;                                           // SEI
      case 0x79: ADC(absolute_index_y()); break;                         // ADC Absolute, Y
      case 0x7D: ADC(absolute_index_x()); break;                         // ADC Absolute, X
      case 0x7E: ROR(absolute_index_x(false)); tick(); break;            // ROR Absolute, X
      case 0x81: STR(pre_indexed(), m_reg_a); break;                     // STA (Indirect, X)
      case 0x84: STR(zero_page(), m_reg_y); break;                       // STY Zero Index 
      case 0x85: STR(zero_page(), m_reg_a); break;                       // STA Zero Page
      case 0x86: STR(zero_page(), m_reg_x); break;                       // STX Zero Page
      case 0x88: DEXY(m_reg_y); break;                                   // DEY
      case 0x8A: TXR(m_reg_a, m_reg_x); break;                           // TXA
      case 0x8C: STR(absolute(), m_reg_y); break;                        // STY Absolute
      case 0x8D: STR(absolute(), m_reg_a); break;                        // STA Absolute
      case 0x8E: STR(absolute(), m_reg_x); break;                        // STX Absolute
      case 0x90: branch_if_clr(FLAGS::C); break;                         // BCC
      case 0x91: STR(indirect_indexed(false), m_reg_a); tick(); break;   // STA (Indirect), Y
      case 0x94: STR(zero_page_index_x(), m_reg_y); break;               // STY Zero Page, X
      case 0x95: STR(zero_page_index_x(), m_reg_a); break;               // STA Zero Page, X
      case 0x96: STR(zero_page_index_y(), m_reg_x); break;               // STX Zero Page, Y
      case 0x98: TXR(m_reg_a, m_reg_y); break;                           // TYA
      case 0x99: STR(absolute_index_y(false), m_reg_a); tick(); break;   // STA Absolute, Y
      case 0x9A: m_reg_sp = m_reg_x; tick(); break;                      // TXS (no flags)
      case 0x9D: STR(absolute_index_x(false), m_reg_a); tick(); break;   // STA Absoulte, X
      case 0xA0: LDR(m_reg_y, m_reg_pc++); break;                        // LDY Immediate
      case 0xA1: LDR(m_reg_a, pre_indexed()); break;                     // LDA (Indirect, X)
      case 0xA2: LDR(m_reg_x, m_reg_pc++); break;                        // LDX Immediate
      case 0xA4: LDR(m_reg_y, zero_page()); break;                       // LDY Zero Page
      case 0xA5: LDR(m_reg_a, zero_page()); break;                       // LDA Zero Page
      case 0xA6: LDR(m_reg_x, zero_page()); break;                       // LDX Zero Page
      case 0xA8: TXR(m_reg_y, m_reg_a); break;                           // TAY
      case 0xA9: LDR(m_reg_a, m_reg_pc++); break;                        // LDA Immediate
      case 0xAA: TXR(m_reg_x, m_reg_a); break;                           // TAX
      case 0xAC: LDR(m_reg_y, absolute()); break;                        // LDY Absolute
      case 0xAD: LDR(m_reg_a, absolute()); break;                        // LDA Absolute
      case 0xAE: LDR(m_reg_x, absolute()); break;                        // LDX Absoulte
      case 0xB0: branch_if_set(FLAGS::C); break;                         // BCS
      case 0xB1: LDR(m_reg_a, indirect_indexed()); break;                // LDA (Indirect), Y
      case 0xB4: LDR(m_reg_y, zero_page_index_x()); break;               // LDY Zero Page, X
      case 0xB5: LDR(m_reg_a, zero_page_index_x()); break;               // LDA Zero Page, X
      case 0xB6: LDR(m_reg_x, zero_page_index_y()); break;               // LDX Zero Page, Y
      case 0xB8: CLV(); break;                                           // CLV
      case 0xB9: LDR(m_reg_a, absolute_index_y()); break;                // LDA Absolute, Y
      case 0xBA: TXR(m_reg_x, m_reg_sp); break;                          // TSX
      case 0xBC: LDR(m_reg_y, absolute_index_x()); break;                // LDY Absolute, X
      case 0xBD: LDR(m_reg_a, absolute_index_x()); break;                // LDA Absolute, X
      case 0xBE: LDR(m_reg_x, absolute_index_y()); break;                // LDX Absolute, Y
      case 0xC0: CMPAXY(m_reg_y, m_reg_pc++); break;                     // CPY
      case 0xC1: CMPAXY(m_reg_a, pre_indexed()); break;                  // CMP (Indirect, X)
      case 0xC4: CMPAXY(m_reg_y, zero_page()); break;                    // CPY Zero Page
      case 0xC5: CMPAXY(m_reg_a, zero_page()); break;                    // CMP Zero Page
      case 0xC6: DEC(zero_page()); break;                                // DEC Zero Page
      case 0xC8: INXY(m_reg_y); break;                                   // INY
      case 0xC9: CMPAXY(m_reg_a, m_reg_pc++); break;                     // CMP Immediate
      case 0xCA: DEXY(m_reg_x); break;                                   // DEX
      case 0xCC: CMPAXY(m_reg_y, absolute()); break;                     // CPY Absolute
      case 0xCD: CMPAXY(m_reg_a, absolute()); break;                     // CMP Absolute
      case 0xCE: DEC(absolute()); break;                                 // DEC Absolute
      case 0xD0: branch_if_clr(FLAGS::Z); break;                         // BNE
      case 0xD1: CMPAXY(m_reg_a, indirect_indexed()); break;             // CMP (Indirect), Y
      case 0xD5: CMPAXY(m_reg_a, zero_page_index_x()); break;            // CMP Zero Page, X
      case 0xD6: DEC(zero_page_index_x()); break;                        // DEC Zero Page, X
      case 0xD8: CLD(); break;                                           // CLD
      case 0xD9: CMPAXY(m_reg_a, absolute_index_y()); break;             // CMP Absolute, Y
      case 0xDD: CMPAXY(m_reg_a, absolute_index_x()); break;             // CMP Absolute, X
      case 0xDE: DEC(absolute_index_x(false)); tick(); break;            // DEC Absolute, X
      case 0xE0: CMPAXY(m_reg_x, m_reg_pc++); break;                     // CPX Immediate
      case 0xE1: SBC(pre_indexed()); break;                              // SBC (Indirect, X)
      case 0xE4: CMPAXY(m_reg_x, zero_page()); break;                    // CPX Zero Page
      case 0xE5: SBC(zero_page()); break;                                // SBC Zero Page
      case 0xE6: INC(zero_page()); break;                                // INC Zero Page
      case 0xE8: INXY(m_reg_x); break;                                   // INX
      case 0xE9: SBC(m_reg_pc++); break;                                 // SBC Immediate
      case 0xEA: NOP(); break;                                           // NOP
      case 0xEC: CMPAXY(m_reg_x, absolute()); break;                     // CPX Absolute
      case 0xED: SBC(absolute()); break;                                 // SBC Absolute
      case 0xEE: INC(absolute()); break;                                 // INC Absolute
      case 0xF0: branch_if_set(FLAGS::Z); break;                         // BEQ
      case 0xF1: SBC(indirect_indexed()); break;                         // SBC (Indirect), Y
      case 0xF5: SBC(zero_page_index_x()); break;                        // SBC Zero Page, X
      case 0xF6: INC(zero_page_index_x()); break;                        // INC Zero Page, X
      case 0xF8: SED(); break;                                           // SED
      case 0xF9: SBC(absolute_index_y()); break;                         // SBC Absolute, Y
      case 0xFD: SBC(absolute_index_x()); break;                         // SBC Absolute, X
      case 0xFE: INC(absolute_index_x()); tick(); break;                 // INC Absolute, X

      /* Illegal opcodes */
      // NOPs
      case 0x04:
      case 0x44:
      case 0x64: 
                 m_reg_pc++; tick(2); break;
      case 0x0C: m_reg_pc+=2; tick(3); break;
      case 0x14: 
      case 0x34: 
      case 0x54: 
      case 0x74: 
      case 0xD4: 
      case 0xF4: 
                 m_reg_pc++; tick(3); break;
      case 0x1A:
      case 0x3A:
      case 0x5A:
      case 0x7A:
      case 0xDA:
      case 0xFA:
                 tick(); break;
      case 0x80: m_reg_pc++; tick(); break;
      case 0x1C:
      case 0x3C:
      case 0x5C:
      case 0x7C:
      case 0xDC:
      case 0xFC:
                 m_reg_pc+=2; tick(4); break;
      // RLA
      //case 0x33: ROL(indirect_indexed()); m_reg_pc--; AND(indirect_indexed()); break;


      default: fprintf(stderr, "Unsupported opcode! %02X\n", op); if (m_bad_ops++ > 10){ SDL_Delay(1000); exit(1);} break;
    }
  }
}
