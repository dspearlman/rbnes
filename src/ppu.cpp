#include "ppu.h"

namespace rbnes {
  Ppu::Ppu(rbnes_system_t system) :
    m_registers{},
    m_system{system}
  {
    std::function<byte (ppu_addr_t)> read_reg = [this](ppu_addr_t addr){return ppu_read_reg(addr);};
    std::function<void (ppu_addr_t, byte)> write_reg = [this](ppu_addr_t addr, byte data){ppu_write_reg(addr, data);};
    system->set_ppu_rw(read_reg, write_reg);
  }

  byte Ppu::ppu_read_reg(ppu_addr_t addr){
    addr %= 8;
    byte retv = 0;
    switch(addr) {
      // Write only
      case PPUCTRL:
      case PPUMASK:
      case OAMADDR:
      case PPUSCROLL:
      case PPUADDR:
        break;
      // Readable
      case OAMDATA:
        retv = m_reg_oamdata;
        break;
      case PPUSTATUS:
        retv = m_reg_ppustatus;
        break;
      // Data special case
      case PPUDATA:
        // This should call back to system so it can handle PPU memory mapping
        // retv = m_vram[m_reg_ppuaddr_h << 8 || m_reg_ppuaddr_l];
      default:
        break;
    }
    return retv;
  }

  void Ppu::ppu_write_reg(ppu_addr_t addr, byte data){
    addr %= 8;
    switch(addr) {
      // Writable
      case PPUCTRL:
      case PPUMASK:
      case OAMADDR:
      case OAMDATA:
        m_registers[addr] = data;
        break;
      // Read Only
      case PPUSTATUS:
        break;
      // Write Twice
      case PPUSCROLL:
      case PPUADDR:
      // Data special case
      case PPUDATA:
      default:
        break;
    }
  }
}
