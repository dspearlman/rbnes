#include <cstdio>
#include "mapper.h"


namespace rbnes{

  namespace {
    const uint16_t c_16KiB = 16384;
    const uint16_t c_32KiB = 32768;
  }

  Mapper::Mapper(std::vector<byte> prg_rom) :
    m_prg_rom(prg_rom),
    m_is_32K(false)
  {
    if (prg_rom.size() >= c_32KiB){
      m_is_32K = true;
    }
    else if (prg_rom.size() < c_16KiB){
      fprintf(stderr, "PROM is incorrect size (%lu) for NROP/M0/No Mapper\n", prg_rom.size());
      exit(ENOEXEC);
    }

    m_is_32K = (prg_rom.size() > 16768);
  }

  byte Mapper::read(const cpu_addr_t addr){
    // Check to see if we're accessing program ROM
    if(addr & 0x8000){
      return m_prg_rom[address_translate(addr)];
    }
    else{
      return 0;
    }
  }

  uint16_t Mapper::address_translate(const cpu_addr_t addr){
    if(m_is_32K){ // ROM ignores MSB here
      return addr & 0x7FFF;
    }
    else{ // Mirror 16k
      return addr & 0x3FFF;
    }
  }
  
}
