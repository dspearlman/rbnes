#include "system.h"

namespace rbnes{

  namespace{
    const cpu_addr_t PPU_SPACE_START = 0x2000;
    const cpu_addr_t IO_SPACE_START = 0x4000;
    const cpu_addr_t CARTRIDGE_SPACE_START = 0x4018;
  }
  
  rbnes_system::rbnes_system(Cartridge& ines)
      : m_internal_ram{},
        m_mapper{nullptr},
        m_ppu_read_reg{},
        m_ppu_write_reg{}
  {
    // Load program memory
    m_mapper = Mapper::create_mapper(ines.get_prg_rom(), ines.get_mapper());
  }

  byte rbnes_system::cpu_read_memory(cpu_addr_t addr){
    byte retv = 0;
    if(addr < PPU_SPACE_START){
      retv = m_internal_ram[addr % 0x800];
    }
    else if (addr > CARTRIDGE_SPACE_START){
      retv = m_mapper->read(addr);
    }
    else if(addr & PPU_SPACE_START) {
      retv = m_ppu_read_reg(addr);
    }
    // Temporary return 0 while nothing else is mapped
    printf("// Read byte %04X:%02X", addr, retv);
    return retv;
  }

  void rbnes_system::cpu_write_memory(const cpu_addr_t addr, const byte data){

    if(addr == 0x02 || addr == 0x03){ // TODO Temporary for debug mode, later enable as a test option for unit testing
      printf("\nAddress 0x%02X: %02X \n", addr, data);
      exit(1);
    }

    printf("// Write byte %02x:%04X", data, addr);
    
    if(addr < PPU_SPACE_START){
      m_internal_ram[addr & 0x7FF] = data;
    }
    else if(addr > CARTRIDGE_SPACE_START){
      m_mapper->write(addr, data);
    }
    else if(addr & PPU_SPACE_START) {
      m_ppu_write_reg(addr, data);
    }
    else {
      printf("\nWrote to unmapped memory!");
    }
  }

  void rbnes_system::set_ppu_rw(std::function<byte (ppu_addr_t)> read, std::function<void (ppu_addr_t, byte)> write) {
    m_ppu_read_reg = read;
    m_ppu_write_reg = write;
  }
}
