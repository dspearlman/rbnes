#include "display.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 960


namespace rbnes {
  Display::Display(){
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
      fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
    }
    m_window = SDL_CreateWindow(
            "hello_sdl2",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            SCREEN_WIDTH, SCREEN_HEIGHT,
            SDL_WINDOW_SHOWN
            );
    if (m_window == NULL) {
      fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
    }
    m_screen_surface = SDL_GetWindowSurface(m_window);
    SDL_FillRect(m_screen_surface, NULL, SDL_MapRGB(m_screen_surface->format, 0xFF, 0xFF, 0xFF));
    SDL_UpdateWindowSurface(m_window);
  }

  Display::~Display() {
    SDL_DestroyWindow(m_window);
    SDL_Quit();
  }
   
}

